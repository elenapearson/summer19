// ***********************************************************
// This example plugins/index.js can be used to load plugins
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const fs = require('fs-extra')
const path = require('path')

function getConfigurationByFile(file) {
  const pathToConfigFile = path.resolve('cypress', 'config', `${file}.json`)

  return fs.readJson(pathToConfigFile)
}

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  const file = config.env.configFile || 'Cypress.env'
  const baseUrl = process.env.CYPRESS_baseUrl
  const password = process.env.CYPRESS_password
  const username = process.env.CYPRESS_username

  return getConfigurationByFile(file)
  return baseUrl
  return password
  return username
}
