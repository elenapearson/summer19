import React from "react";
import { connect } from "react-redux";
import { startLogin } from "../actions/auth";
//when the btn is clicked, we just call the startLogin prop
//we can just destructure  that from props ({startLogin})
export const LoginPage = ({ startLogin }) => (
  <div className="bg-layout">
    <div className="bg-layout__box">
      <h1 className="bg-layout__title">Example Toolbox</h1>
      <h4>Please login to continue</h4>
    <button className="button" onClick={startLogin}>Login</button>
    </div>
  </div>
);

const mapDispatchToProps = dispatch => ({
  startLogin: () => dispatch(startLogin())
});

export default connect(
  undefined,
  mapDispatchToProps
)(LoginPage);
