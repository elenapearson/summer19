import React from 'react';

const LoadingPage = () => (
  <div className="loader center">
    <i alt="loader" className="fa fa-cog fa-spin" />
  </div>
);

export default LoadingPage;
