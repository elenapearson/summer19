import React from "react";
import { Link } from "react-router-dom";

const NotFoundPage = () => (
  <div>
    <h1>404</h1>
    <h4>Page not found</h4>
    <h2>
      <Link to="/">Find the Dashboard Here:</Link>
    </h2>
  </div>
);

export default NotFoundPage;
