import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { startLogout } from "../actions/auth";

export const Header = ({ startLogout }) => (
  <div className="header-bg">
    <div className="container">
      <div className="header__content">
      <Link className="header__title" to="/dashboard" exact={true}>
      <h1>Dashboard</h1>
    </Link>
<button className="button--link" onClick={startLogout}><div className="">Logout</div></button>
      </div>
    </div>
  </div>
);

const mapDispatchToProps = dispatch => ({
  startLogout: () => dispatch(startLogout())
});

export default connect(
  undefined,
  mapDispatchToProps
)(Header);
