// long running asynchronous task
//any type of long running task like request to a server,
//or trying to take a picture with a webcam, or ...
//this part is done for us by firebase
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("This is my resolved data");
  }, 1500);
});
console.log("before");
//promises are a way to synk out asyncronous operation
//this part we will do ourselves and include in our application:
promise
  .then(data => {
    console.log("1", data);
  })
  .catch(error => {
    console.log("error", error);
  });
console.log("after");
