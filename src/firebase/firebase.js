import * as firebase from "firebase";

// Initialize Firebase */}
var config = {
  apiKey: "AIzaSyC_VR9Yqcx0ZIEV6Y2S3kADAJjo4z-kdPg",
  authDomain: "expensestracker-8c9d2.firebaseapp.com",
  databaseURL: "https://expensestracker-8c9d2.firebaseio.com",
  projectId: "expensestracker-8c9d2",
  storageBucket: "expensestracker-8c9d2.appspot.com",
  messagingSenderId: "373467920670"
};
firebase.initializeApp(config);

const database = firebase.database();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, googleAuthProvider, database as default };
// //child_removed event
// database.ref("expenses").on("child_removed", snapshot => {
//   console.log(snapshot.key, snapshot.val());
// });
// //child_changed event
// database.ref("expenses").on("child_changed", snapshot => {
//   console.log(snapshot.key, snapshot.val());
// });
// //child_added
// database.ref("expenses").on("child_added", snapshot => {
//     console.log(snapshot.key, snapshot.val());
//   });
// database.ref("expenses").on("value", snapshot => {
//   const expenses = [];
//   snapshot.forEach(childSnapshot => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     });
//   });
//   console.log(expenses);
// });
// database
//   .ref("expenses")
//   .once("value")
//   .then(snapshot => {
//     const expenses = [];
//     snapshot.forEach(childSnapshot => {
//       expenses.push({
//         id: childSnapshot.key,
//         ...childSnapshot.val()
//       });
//     });
//     console.log(expenses);
//   });
// database.ref("expenses").push({
//   description: "Rent",
//   note: "",
//   amount: 5900,
//   createdAt: 877299020
// });

// database.ref("expenses").push({
//   description: "Phone Bill",
//   note: "",
//   amount: 14900,
//   createdAt: 877299021
// });

// database.ref("expenses").push({
//   description: "Food",
//   note: "",
//   amount: 24900,
//   createdAt: 877299021
// });

// database.ref("notes/-L_dmkDVllSYnX1DetfQ").update({
//   body: "some things we should know about "
// });

// database.ref("notes").push({
//   title: "To do",
//   topics: "Finish course 5"
// });

// database.ref("notes").push({
//   title: "Course Topics",
//   topics: "React, HTML, CSS"
// });

// const firebaseNotes = {
//   notes: {
//     udhusfhusfhs: {
//       title: "First note",
//       body: "this is my first note"
//     }
//   }
// };
// const notes = [
//   {
//     id: "12",
//     title: "First note",
//     body: "this is my 1st note"
//   },
//   {
//     id: "12",
//     title: "Second note",
//     body: "this is 2nd note"
//   }
// ];
// database.ref("notes").set(notes);
//to fetch all of the data
// database
//   .ref()
//   .once("value")
//   .then(snapshot => {
//     const val = snapshot.val();
//     console.log(val);
//   })
//   .catch(e => {
//     console.log("error fetching data");
//   });
//listens to changes in the database and subscribes for changes
// database.ref().on(
//   "value",
//   snapshot => {
//     const val = snapshot.val();
//     console.log(`${val.name} is a ${val.job.title} at ${val.job.company}`);
//   },
//   e => {
//     console.log("Error with data fetching", e);
//   }
// );
// setTimeout(() => {
//   database.ref("age").set(25);
// }, 3500);

// setTimeout(() => {
//   database.ref("age").set(30);
// }, 10500);
// we are using the ref and set methods to set up promises with firebase
//if javascript disn't support asynchronous programming
// then we will be locking the browser up
//and the user won't be able to interact with the form fields
// database
//   .ref()
//   .set({
//     name: "Elena A P",
//     age: 35,
//     stressLevel: 6,
//     isSingle: false,
//     location: {
//       city: "Bellevue",
//       country: "United States"
//     },
//     job: {
//       title: "Developer",
//       company: "Google"
//     }
//   })
//   .then(() => {
//     console.log("data is saved");
//   })
//   .catch(e => {
//     console.log("this failed", e);
//   });

// database.ref().update({
//   name: "Steph",
//   age: 33,
//   "job/title": "Software Developer",
//   isSingle: null,
//   "location/city": "Duvall"
// });
// database.ref("age").set(36);
// database.ref("location/city").set("Bothell");

// database
//   .ref("attributes")
//   .set({
//     height: 164,
//     weight: 50
//   })
//   .then(() => {
//     console.log("second set call worked");
//   })
//   .catch(e => {
//     console.log("second set call failed with error", e);
//   });

// console.log("I made a request to change the data");
// database.ref('isSingle')

// database
//   .ref("isSingle")
//   .remove()
//   .then(function() {
//     console.log("Remove succeeded.");
//   })
//   .catch(function(error) {
//     console.log("Remove failed: " + error.message);
//   });

// database
//   .ref()
//   .remove()
//   .then(function() {
//     console.log("Remove succeeded.");
//   })
//   .catch(function(error) {
//     console.log("Remove failed: " + error.message);
//   });
